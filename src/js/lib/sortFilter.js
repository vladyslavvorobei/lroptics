export default () => {

    const $button = $('.sort-filter__button');
    const activeClass = 'sort-filter__button--active'
    const activeUpClass = 'sort-filter__button--up'
    const activeDownClass = 'sort-filter__button--down'

    $button.on('click', function(e) {
        const $thisButton = $(this);
        e.preventDefault();
        if ($thisButton.hasClass(activeClass)){
            $thisButton.addClass(activeClass).addClass(activeUpClass);
            if ($thisButton.hasClass(activeDownClass)) {
                $thisButton.removeClass(activeDownClass).addClass(activeUpClass)
            } else {
                $thisButton.removeClass(activeUpClass).addClass(activeDownClass)
            }
        } else {
            $button.removeClass(activeClass).removeClass(activeUpClass).removeClass(activeDownClass);
            $thisButton.addClass(activeClass).addClass(activeUpClass);
        }
    })
    
}