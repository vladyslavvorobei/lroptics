export default () => {

    // search
    const searchField = $('.header__search-field');
    const activeClassSearchField = 'header__search-field--active';

    searchField.on('focus', function () {
        searchField.addClass(activeClassSearchField)
    })
    searchField.on('focusout', function () {
        searchField.removeClass(activeClassSearchField)
    })

    const headerNav = $('.header__nav');
    const headerNavActiveClass = 'header__nav--active' 
    const headerNavLink = $('.header__nav a');
    const btn = $('.header__mobile-btn');
    const btnClassActive = 'header__mobile-btn--active';

    btn.on('click', function() {
        headerNav.toggleClass(headerNavActiveClass);
        btn.toggleClass(btnClassActive);
    })
    headerNavLink.on('click', function() {
        headerNav.removeClass(headerNavActiveClass);
        btn.removeClass(btnClassActive);
    })


    const headerMenuList = $('.header__menu li');
    const hiddenHeaderMenuList = $('.header__menu-hidden li');

    function menuSize () {

        if ($(window).width() <= 991) {
            headerMenuList.show();
        } else if ( $(window).width() > 991 && $(window).width() <= 1199 ) {
            headerMenuList.show();
            hiddenHeaderMenuList.show();
            headerMenuList.slice(3).hide();
            hiddenHeaderMenuList.slice(0, 3).hide();
        } else if (  $(window).width() > 1199 && $(window).width() <= 1499 ){
            headerMenuList.show();
            hiddenHeaderMenuList.show();
            headerMenuList.slice(5).hide();
            hiddenHeaderMenuList.slice(0, 5).hide()
        } else if ( $(window).width() > 1499 ) {
            headerMenuList.show();
            hiddenHeaderMenuList.show();
            headerMenuList.slice(7).hide();
            hiddenHeaderMenuList.slice(0, 7).hide()
        }
    }
    
    menuSize();

    $(window).on('resize', function() {
        menuSize();
        if ($(window).width() > 991) {
            headerNav.removeClass(headerNavActiveClass);
            btn.removeClass(btnClassActive);
        }
    })

}