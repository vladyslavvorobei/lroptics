import Swiper from 'swiper';
import { gsap } from 'gsap';

export default  (duration = 30) =>  {
    if ($('*').is('.intro')) {
        const circle = document.querySelector('.intro-progress__circle')
        const radius = circle.r.baseVal.value;
        const circumference = 2 * Math.PI * radius;

        circle.style.strokeDasharray = `${circumference} ${circumference}`;
        circle.style.strokeDashoffset = circumference;

        const buttonNext = '.intro__button-next';

        function triggerNextSlide () {
            $(buttonNext).trigger('click');
        }
        const tween = gsap.to('.intro-progress__circle', {duration, strokeDashoffset: 0, ease: "none", onComplete: triggerNextSlide}  );

        $('.intro').on('mousedown taphold', function (){
            tween.pause();
        })
        $('.intro').on('mouseup', function (){
            tween.play();
        })
        const introSwiper = new Swiper ('.intro__slider.swiper-container', {
            autoHeight: true,
            navigation: {
                nextEl: buttonNext,
                prevEl: '.intro__button-prev',
            },
            on: {
                slideChange() {
                    tween.restart();
                }
            }
        })
    }
}

