export default () => {
    const $card = $('.product-grid__card'); 
    const imgClassActive = 'product-grid__img--active';
    const colorClassActive = 'product-grid__color--active';
    const priceClassActive = 'product-grid__price--active';

    $('.product-grid__tags').on('click', function(e){
        e.preventDefault();
    })
    
    for (let i = 0; i < $card.length; i++) {

        const $cardImg = $card.eq(i).find('.product-grid__img');
        $cardImg.eq(0).addClass(imgClassActive);

        const $cardColor = $card.eq(i).find('.product-grid__color');
        $cardColor.eq(0).addClass(colorClassActive);
       
        const $cardPrice = $card.eq(i).find('.product-grid__price');
        $cardPrice.eq(0).addClass(priceClassActive);
    }

    $('.product-grid__color').on('mouseover', function(e) {
        e.preventDefault();
        $(this)
        .addClass(colorClassActive).siblings().removeClass(colorClassActive).closest('.product-grid__card').find('.product-grid__img').removeClass(imgClassActive).eq($(this).index()).addClass(imgClassActive).closest('.product-grid__card').find('.product-grid__price').removeClass(priceClassActive).eq($(this).index()).addClass(priceClassActive);
    });

    const $color = $('.product-grid__color');
    const $productColor = $('.product-card__color');

    function addStyleColor (item) {
        for (let i = 0; i < item.length; i++) {
            const thisColor = item.eq(i);
            const dataAttr = thisColor.data('color');
            thisColor.css('background-color',dataAttr);
        }
    }

    addStyleColor($color);
    addStyleColor($productColor);
    
    const loadMoreBtn = $('.product-grid__load-more');

    if ($card.length <= 8) {
        loadMoreBtn.remove()
    } else {
        $card.slice(0, 7).show();
        $card.slice(7).hide();
    }
    
    loadMoreBtn.on('click', function(e) {
        e.preventDefault();
        $('.product-grid__card:hidden').slice(0,4).slideDown();
        if ($('.product-grid__card:hidden').length === 0) {
            loadMoreBtn.hide();
        } else {
            loadMoreBtn.show();
        }
    });


}