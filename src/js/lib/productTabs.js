export default () => {
    const title = $('.card-product__tab-title');
    const titleClass = 'card-product__tab-title--active';
   
    function addClass() {
        if ($(this).hasClass(titleClass)) {
            $(this).removeClass(titleClass);
        } else {
            $(this).addClass(titleClass);
        }
        
        $(this).next().slideToggle();
    }
    
    title.click(addClass);
}