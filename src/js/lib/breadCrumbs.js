export default () => {
    if ($('*').is('.bread-crumbs')) {
        const $breadCrumbs = $('.bread-crumbs');
        $breadCrumbs.next().addClass('bread-crumbs__padding-top');
    }
}
