export default () => {
    const $gridLine = $('.grid-line');
    $gridLine.append('<div class="grid-line__wrapper">');
    const  $gridLineWrapper = $('.grid-line__wrapper');
    let screenWidth = $(window).innerWidth();

    function addLine (number) {
        for (let i = 0; i < number; i++ ) {
            $gridLineWrapper.append('<span class="grid-line__line"></span>');
        }
    }

    function checkWidth () {
        $('.grid-line__line').remove()
        if (screenWidth >= 1500) {
            addLine(5);
        } else if (screenWidth >= 1200 && screenWidth <= 1499) {
            addLine(4);
        } else if (screenWidth > 575 && screenWidth <= 1199) {
            addLine(3);
        } else {
            addLine(2);
        }
    }

    checkWidth();

    $(window).resize(function() {
        screenWidth = $(window).innerWidth();
        checkWidth();
    });
}