import Swiper from 'swiper';

export default () => {
    var swiper = new Swiper('.card-popup__gift-slider .swiper-container', {
        slidesPerView: 1,
        spaceBetween: 20,
        loop: true,
        navigation: {
            nextEl: '.card-popup__button--next',
            prevEl: '.card-popup__button--prev',
        },
        breakpoints: {
            575: {
                slidesPerView: 2,
                spaceBetween: 8,
            },
            992: {
                slidesPerView: 3,
                spaceBetween: 8,
            },
            1199: {
                slidesPerView: 4,
                spaceBetween: 8,
            },
            1499: {
                slidesPerView: 3,
                spaceBetween: 8,
            },
            1770: {
                slidesPerView: 4,
                spaceBetween: 8,
            }
        }
    });

    (function () {
        const $parent = $('.card-popup__input');
        const $input = $('.card-popup__input input');
        const $inputUp = $('.card-popup__input-up');
        const $inputDown = $('.card-popup__input-down');
    

        function customNumberArrows (button, arg) {
            button.on('click', function(e) {
                e.preventDefault();
                const findInput = $(this).closest($parent).find('input')[0];
                $(this).closest($parent).find('label').addClass('active');
                if (arg === 'up') {
                    findInput.stepUp();
                } else if (arg === 'down') {
                    findInput.stepDown();
                }
            })
        }

        customNumberArrows($inputUp, 'up')
        customNumberArrows($inputDown, 'down')


        $input.on('focus', function() {
          $(this).next($input).addClass('active');
        });
        

        $($input).on('focusout',function() {
          '' === $(this).val() && $(this).next($input).removeClass('active');
        });


        const openPopupBtn = $('.card-product__add-to-cart');
        const closePopupBtn = $('.card-popup__close-btn');
        const popup = $('.card-popup');
        const popupOpen = 'card-popup--open'
    
        openPopupBtn.on('click', function() {
            popup.addClass(popupOpen);
            swiper.update();
        })
        closePopupBtn.on('click', function() {
            popup.removeClass(popupOpen);
            swiper.update();
        })

      
    })()
}