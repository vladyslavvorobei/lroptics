import Swiper from 'swiper';

export default () => {
    const reviewsSwiper = new Swiper ('.reviews__slider.swiper-container', {
        autoHeight: true,
        navigation: {
            nextEl: '.reviews__button--next',
            prevEl: '.reviews__button--prev',
        },
        autoplay: {
            delay: 3000,
            disableOnInteraction: false,
        },
    })
}