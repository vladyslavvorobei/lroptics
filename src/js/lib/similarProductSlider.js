import Swiper from 'swiper';

export default () => {
    var swiper = new Swiper('.similar-product__slider .swiper-container', {
        slidesPerView: 1,
        navigation: {
            nextEl: '.similar-product__button--next',
            prevEl: '.similar-product__button--prev',
        },
        breakpoints: {
            1199: {
              slidesPerView: 2
            },
            1499: {
              slidesPerView: 3
            },
          }
    });
}