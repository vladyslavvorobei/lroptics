
export default () => {
    const $item = $('.services-grid__item');
    for (let i = 0; i < $item.length; i++) {
        const thisItem = $item.eq(i);
        const bgImg = thisItem.data('bgimg');
        thisItem.css('background-image','url("' + bgImg + '")');
    }
}