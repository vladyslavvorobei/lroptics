export default () => {
    
    const $buttonBack = $('.bread-crumbs__back');

    $buttonBack.on('click', function() {
        window.history.back();
    })

}