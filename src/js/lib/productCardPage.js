import Swiper from 'swiper';

export default () => {
    var swiper = new Swiper('.product-card__slider .swiper-container', {
        loop: true,
        spaceBetween: 30,
        navigation: {
          nextEl: '.product-card__slider-button--next',
          prevEl: '.product-card__slider-button--prev',
        },
      });
}