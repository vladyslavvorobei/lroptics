import header from './lib/header';
import introSlider from './lib/introSlider';
import productGrid from './lib/productGrid';
import servicesGrid from './lib/servicesGrid';
import reviewsSlider from './lib/reviewsSlider';
import addGrid from './lib/addGrid';
import breadCrumbs from './lib/breadCrumbs';
import buttonBack from './lib/buttonBack';
import customSelect from './lib/customSelect';
import sortFilter from './lib/sortFilter';
import productCardPage from './lib/productCardPage';
import productTabs from './lib/productTabs';
import similarProductSlider from './lib/similarProductSlider';
import cardPopup from './lib/cardPopup';



$(document).ready(function() {
    header();
    addGrid();
    introSlider(3);
    productGrid();
    servicesGrid();
    reviewsSlider();
    breadCrumbs();
    buttonBack();
    sortFilter();
    customSelect();
    productCardPage();
    productTabs();
    similarProductSlider();
    cardPopup();
})

